import React, { createContext } from "react";
import API from "../../utils/API.js";
const MyContext = createContext();
const Provider = MyContext.Provider;
const Consumer = MyContext.Consumer;

class ConfigProvider extends React.PureComponent {
  state = {
    data: [],
    loading: true,
    error: "",
    contract: "",
  };

  async componentDidMount() {
    this.fetchData();
  }

  fetchData = async () => {
    try {
      let res = await API.get(`5ed511c53300005f00f7a790`);
      if (res.status === 200) {
        console.log(res);
        let data = await res.data;
        this.setState({
          data,
          error: "",
          contract: data.contract_length.preselected_contract_length,
          loading: false,
        });
      }
    } catch (error) {
      console.log(error);
      this.setState({ error });
    }
  };

  onChangeHandler = async (e) => {
    this.setState({ contract: e.target.value });
  };

  render() {
    return (
      <Provider
        value={{
          state: this.state,
          onChangeHandler: (e) => this.onChangeHandler(e),
        }}
      >
        {this.props.children}
      </Provider>
    );
  }
}

export { MyContext, Provider, ConfigProvider };
export default Consumer;
