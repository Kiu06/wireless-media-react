import React from "react";
import Consumer from "../../components/provider/Provider";
import PackageCard from "../../partials/packageCard/PackageCard";
import Loader from "../../partials/loader/Loader";
import Dropdown from "../../partials/dropdown/Dropdown";

const Home = () => {
  return (
    <Consumer>
      {(ctx) => (
        <div className="container homeWrap">
          <div className="dropdownWrap">
            <Dropdown />
          </div>
          <div className="row packageCardRow">
            {ctx.state.loading ? (
              <Loader />
            ) : (
              ctx.state.data.items.map((item) => (
                <PackageCard
                  key={item.id}
                  data={ctx.state.data}
                  item={item}
                  contract={ctx.state.contract}
                />
              ))
            )}
          </div>
        </div>
      )}
    </Consumer>
  );
};

export default Home;
