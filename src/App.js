import React, { Suspense } from "react";
import Home from "./components/home/Home";
import Loader from "./partials/loader/Loader";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { ConfigProvider } from "./components/provider/Provider";
import "./assets/scss/styles.scss";

function App() {
  return (
    <ConfigProvider>
      <Router>
        <div className="App">
          <Suspense fallback={<Loader />}>
            <Switch>
              <Route path="/" exact component={Home} />
            </Switch>
          </Suspense>
        </div>
      </Router>
    </ConfigProvider>
  );
}

export default App;
