import React from "react";
import PropTypes from "prop-types";

const Features = ({ list, category }) => {
  function boldString(s, b) {
    return s.replace(RegExp(b, "g"), `<b>${b}</b>`);
  }

  return (
    <div className="featuresWrap">
      <ul>
        {list.map(
          (featureItem) =>
            featureItem.product_category === category && (
              <li key={featureItem.id}>
                {boldString(
                  featureItem.long_name,
                  featureItem.attributes.attribute_value
                )}
              </li>
            )
        )}
      </ul>
    </div>
  );
};

Features.propTypes = {
  list: PropTypes.array.isRequired,
  category: PropTypes.string.isRequired,
};

export default Features;
