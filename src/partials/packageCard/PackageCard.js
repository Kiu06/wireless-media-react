import React from "react";
import Consumer from "../../components/provider/Provider";
import Features from "../../partials/features/Features";
import PropTypes from "prop-types";

const PackageCard = ({ item, contract }) => {
  return (
    <Consumer>
      {(ctx) => (
        <div className="packageCardWrap col-12 col-md-4">
          <div className="packageCardInner">
            <div className={item.is_featured ? "featured" : "featured hidden"}>
              <p>{ctx.state.data.promo_text}</p>
            </div>
            <h1>{item.name}</h1>
            <div className="tvIncludes">
              <div className="img-wrap">
                <img
                  src="../../../images/tv-logo.svg"
                  fluid="true"
                  className="noselect"
                  alt="tv-logo"
                />
              </div>
              <Features list={item.included} category="tv" />
            </div>
            <div className="netIncludes">
              <div className="img-wrap">
                <img
                  src="../../../images/mobile-logo.svg"
                  fluid="true"
                  className="noselect"
                  alt="mobile-logo"
                />
              </div>
              <Features list={item.included} category="net" />
              {contract === "Ugovor 24 meseca" && item.promotions && (
                <div className="promotions">
                  {item.promotions.map((promotionItem) => (
                    <div className="promotionsInner" key={promotionItem.id}>
                      <img
                        src="../../../images/tv.png"
                        className="noselect"
                        alt="fox-tv"
                      />
                      <p>
                        {JSON.stringify(promotionItem.promo_text).slice(4, -5)}
                      </p>
                    </div>
                  ))}
                </div>
              )}
            </div>
            <div className="priceWrap">
              <div className="priceInner">
                {console.log(item.prices.old_price_recurring[contract])}
                <h4 className="crossout">
                  {parseFloat(
                    item.prices.old_price_recurring[contract]
                  ).toFixed(0) + " rsd/mes."}
                </h4>
                <h3>
                  {parseFloat(item.prices.price_recurring[contract]).toFixed(
                    0
                  ) + " rsd/mes."}
                </h3>
              </div>
              <p>
                {JSON.stringify(item.prices.old_price_promo_text).slice(4, -5)}
              </p>
              <button>Naručite</button>
            </div>
          </div>
        </div>
      )}
    </Consumer>
  );
};

PackageCard.propTypes = {
  item: PropTypes.object.isRequired,
  contract: PropTypes.string,
};

export default React.memo(PackageCard);
