import React from "react";
import Consumer from "../../components/provider/Provider";

const Dropdown = () => {
  return (
    <Consumer>
      {(ctx) => (
        <div>
          {ctx.state.loading === false && (
            <select
              className="dropdown"
              defaultValue={ctx.state.contract}
              onChange={ctx.onChangeHandler}
            >
              {ctx.state.data.contract_length.contract_length_options.map(
                (contract) => (
                  <option key={contract} value={contract}>
                    {contract}
                  </option>
                )
              )}
            </select>
          )}
        </div>
      )}
    </Consumer>
  );
};

export default Dropdown;
