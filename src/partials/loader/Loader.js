import React from "react";

const Loader = () => {
  return (
    <div className="loader-wrap">
      <div className="spinner-wrap">
        <div className="lds-dual-ring" />
      </div>
    </div>
  );
};

export default Loader;
