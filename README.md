# WIRELESS MEDIA FE TEST

Repository for WIRELESS MEDIA FE test.
Built width ReactJS, Sass, Bootstrap.

## Available Scripts

In the project directory, you can run:

```
$ npm i
$ npm start
```

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
